import { Home, Circle } from 'react-feather'

export default [
  {
    id: 'dashboards',
    title: 'Dashboards',
    icon: <Home size={20} />,
    // badge: 'light-warning',
    //     // badgeText: '2',
    children: [
      {
        id: 'crac',
        title: 'CRAC',
        icon: <Circle size={12} />,
        navLink: '/dashboard/CRAC',
        action: 'manage',
        resource: 'ACL'
      },
      {
        id: 'dehum',
        title: 'Dehum',
        icon: <Circle size={12} />,
        navLink: '/dashboard/DEHUM'
      },
      {
        id: 'ahu',
        title: 'AHU',
        icon: <Circle size={12} />,
        navLink: '/dashboard/AHU'
      },
      {
          id: 'cpos',
        title: 'CPOS',
        icon: <Circle size={12} />,
        navLink: '/dashboard/CPOS'
      },
      {
        id: 'tank',
        title: 'Tank',
        icon: <Circle size={12} />,
        navLink: '/tank/All'
      } ,
      {
        id: 'Outdoor Temp',
        title: 'Outdoor Temp',
        icon: <Circle size={12} />,
        navLink: '/outdoor/All'
      } ,
      {
        id: 'Hydrogen',
        title: 'Hydrogen',
        icon: <Circle size={12} />,
        navLink: '/hydrogen/All'
      },
      {
        id: 'SupplyCrac',
        title: 'Supply Crac',
        icon: <Circle size={12} />,
        navLink: '/supply/All'
      },
      {
        id: 'pump',
        title: 'Pump',
        icon: <Circle size={12} />,
        navLink: '/pump/All'
      },

      {
        id: 'busbar',
        title: 'Busbar',
        icon: <Circle size={12} />,
        navLink: '/bar/details/busbar/jk2'
      },

      {
        id: 'idf',
        title: 'IDF & MDF',
        icon: <Circle size={12} />,
        navLink: '/idf/jk1'
      },


      {
        id: 'ct',
        title: 'Cooling Tower',
        icon: <Circle size={12} />,
        navLink: '/ct/All'
      },

      {
        id: 'other',
        title: 'Others',
        icon: <Circle size={12} />,
        navLink: '/other/All'
      }

    ]
  }
]
