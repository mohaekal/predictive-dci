// ** React Imports
import { Fragment } from 'react'
import * as Icon from 'react-feather'

// ** Custom Components
import NavbarUser from './NavbarUser'
import NavbarBookmarks from './NavbarBookmarks'
import {NavItem, NavLink} from "reactstrap";
import NavbarSearch from "./NavbarSearch";
import {Moon, Sun} from "react-feather";

const ThemeNavbar = props => {
  // ** Props
  const { skin, setSkin, setMenuVisibility } = props

    const ThemeToggler = () => {
        if (skin === 'dark') {
            return <Sun className='ficon' onClick={() => setSkin('light')} />
        } else {
            return <Moon className='ficon' onClick={() => setSkin('dark')} />
        }
    }


  return (
    <Fragment>
      <div className='bookmark-wrapper d-flex align-items-center'>
          <NavItem className='mobile-menu d-none d-lg-block'>
              <NavLink className='nav-link-style'>
                  <ThemeToggler />

              </NavLink>
          </NavItem>

          <ul className='navbar-nav d-xl-none'>
              <NavItem className='mob/details/temperatureile-menu mr-auto'>
                  <NavLink className='nav-menu-main menu-toggle hidden-xs is-active' onClick={() => setMenuVisibility(true)}>
                      <Icon.Menu className='ficon' />
                  </NavLink>
              </NavItem>
          </ul>

      </div>
      <NavbarUser skin={skin} setSkin={setSkin} />
    </Fragment>
  )
}

export default ThemeNavbar
