import mock from '../mock'

export const searchArr = [
  {
    groupTitle: 'Pages',
    searchLimit: 4,
    data: [
      {
        id: 1,
        target: 'trend',
        isBookmarked: false,
        title: 'trend',
        icon: 'Home',
        link: '/dashboard/trend'
      },

      {
        id: 2,
        target: 'Cpos Details',
        isBookmarked: false,
        title: 'Cpos Details',
        icon: 'Cpu',
        link: '/cpos/jk1/Chiller_3'
      }
    ]
  },
  {
    groupTitle: 'Trends',
    searchLimit: 4,
    data: [
      {
        title: 'Mia Davis'
      },
      {
        title: 'Norris Carrière'
      }
    ]
  }
]

// GET Search Data
mock.onGet('/api/main-search/data').reply(config => {
  return [200, { searchArr }]
})

// GET Search Data & Bookmarks
mock.onGet('/api/bookmarks/data').reply(config => {
  const bookmarks = searchArr[0].data.filter(item => item.isBookmarked)
  const suggestions = searchArr[0].data
  return [200, { suggestions, bookmarks }]
})

// POST Update isBookmarked
mock.onPost('/api/bookmarks/update').reply(config => {
  const { id } = JSON.parse(config.data)

  const obj = searchArr[0].data.find(item => item.id === id)

  Object.assign(obj, { isBookmarked: !obj.isBookmarked })

  return [200]
})
