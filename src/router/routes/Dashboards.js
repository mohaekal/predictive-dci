import { lazy } from 'react'

const DashboardRoutes = [
  // Dashboards

  {
    path: '/login',
    component: lazy(() => import('../../views/pages/authentication/Login')),
    layout: 'BlankLayout',
    meta: {
      authRoute: true
    }
  },

  {
    path: '/dashboard/',
    component: lazy(() => import('../../views/pages/dashboard/base')),
    exact: true
  },

  {
    path: '/dashboard/CRAC',
    component: lazy(() => import('../../views/pages/dashboard/table/tableBase')),
    exact: true
  },

  {
    path: '/dashboard/DEHUM',
    component: lazy(() => import('../../views/pages/dashboard/table/tableBase')),
    exact: true
  },

  {
    path: '/dashboard/AHU',
    component: lazy(() => import('../../views/pages/dashboard/table/tableBase')),
    exact: true
  },

  {
    path: '/dashboard/CPOS',
    component: lazy(() => import('../../views/pages/dashboard/table/tableBase')),
    exact: true
  },

  {
    path: '/dashboard/summary',
    component: lazy(() => import('../../views/pages/summary/summ')),
    exact: true
  },
  {
    path: '/dashboard/bar',
    component: lazy(() => import('../../views/pages/dashboard/bar/BarDetail')),
    exact: true
  },

  {
    path: '/dashboard/trend/temperature',
    component: lazy(() => import('../../views/pages/trend/temperature/trend')),
    exact: true
  },

  {
    path: '/dashboard/trend/vibration',
    component: lazy(() => import('../../views/pages/trend/vibration/trend')),
    exact: true
  },

  {
    path: ['/bar/M','/bar/Level_1_A','/bar/Level_1_b','/bar/Level_2_b','/bar/Level_3_A','/bar/Level_3_B'],
    component: lazy(() => import('../../views/pages/dashboard/bar/BarDetail')),
    exact: true
  },

  {
    path: ['/cpos/jk1','/cpos/jk1/Chiller_1','/cpos/jk1/Chiller_2','/cpos/jk1/Chiller_3','/cpos/jk1/Chiller_4'],
    component: lazy(() => import('../../views/pages/dashboard/cpos/jk-1/TableCposDetailsJk1')),
    exact: true
  } ,
  {
    path: ['/cpos/jk2','/cpos/jk2/Chiller%20JK2-A','/cpos/jk2/Chiller%20JK2-B'],
    component: lazy(() => import('../../views/pages/dashboard/cpos/jk-2/TableCposDetailsJk2'))
  },
  {
    path: ['/cpos/jk3','/cpos/jk3/Chiller%201-1','/cpos/jk3/Chiller%201-2','/cpos/jk3/Chiller%201-3','/cpos/jk3/Chiller-2', '/cpos/jk3/Chiller-3','/dashboard/cpos/jk3/Chiller-4'],
    component: lazy(() => import('../../views/pages/dashboard/cpos/jk-3/TableCposDetailsJk3'))
  },

  {
    path: ['/bar/details','/bar/details/temperature','/bar/details/vibration','/bar/details/Suc_Pressure'],
    component: lazy(() => import('../../views/pages/dashboard/table/details/CracTable')),
    exact: true
  },

  {
    path: ['/tank/All','/tank/Ground','/tank/Basement','/tank/Roof'],
    component: lazy(() => import('../../views/pages/dashboard/waterTank/base')),
    exact: true
  },

  {
    path: ['/outdoor/All','/outdoor/Ground','/outdoor/Basement','/outdoor/Roof'],
    component: lazy(() => import('../../views/pages/dashboard/outdoor/base')),
    exact: true
  },

  {
    path: ['/ct/All','/ct/Ground','/ct/Basement','/ct/Roof'],
    component: lazy(() => import('../../views/pages/dashboard/coolingtower/base')),
    exact: true
  },

  {
    path: ['/other/All','/other/Ground','/other/Basement','/other/Roof'],
    component: lazy(() => import('../../views/pages/dashboard/other/base')),
    exact: true
  },

  {
    path: ['/test'],
    component: lazy(() => import('../../views/pages/dashboard/test/test1')),
    exact: true
  },

  {
    path: ['/hydrogen/All','/hydrogen/Ground','/hydrogen/Basement','/hydrogen/Roof'],
    component: lazy(() => import('../../views/pages/dashboard/hydrogen/base')),
    exact: true
  },

  {
    path: ['/supply/Crac','/supply/All'],
    component: lazy(() => import('../../views/pages/dashboard/supply/BarDetail')),
    exact: true
  },


  {
    path: ['/pump/Crac','/pump/All'],
    component: lazy(() => import('../../views/pages/dashboard/pump/BarDetail')),
    exact: true
  },

  {
    path: ['/dehum/Crac','/dehum/All'],
    component: lazy(() => import('../../views/pages/dashboard/dehum/dehum')),
    exact: true
  },


  {
    path: ['/dehum/Dehumidifier','/bar/details/Dehum'],
    component: lazy(() => import('../../views/pages/dashboard/table/details/DehumTableTab')),
    exact: true
  },

  // {
  //   path: ['/bar/details/busbar'],
  //   component: lazy(() => import('../../views/pages/dashboard/table/details2/busbar')),
  //   exact: true
  // },

  {
    path: ['/idf/details/jk1'],
    component: lazy(() => import('../../views/pages/dashboard/table/details2/idf/idfjk1')),
    exact: true
  },


  {
    path: ['/idf/details/jk2'],
    component: lazy(() => import('../../views/pages/dashboard/table/details2/idf/idfjk2')),
    exact: true
  },



  {
    path: ['/dehum/Dehumidifiertop','/bar/details/Dehum'],
    component: lazy(() => import('../../views/pages/dashboard/table/details/DehumTableTabTop')),
    exact: true
  },

  {
    path: ['/alerts','/alerts/derivative','/alerts/all','/alerts/Mechanical','/alerts/Electrical','/alerts/general'],
    component: lazy(() => import('../../views/pages/dashboard/table/alert/base')),
    exact: true
  },

  {
    path: ['/ahu/Differential_Pressure','/bar/details/Differential_Pressure'],
    component: lazy(() => import('../../views/pages/dashboard/table/details/AhuDiffTableTab')),
    exact: true
  },

  {
    path: ['/ahu/supply','/bar/details/supply'],
    component: lazy(() => import('../../views/pages/dashboard/table/details/AhuTableTab')),
    exact: true
  },

  {
    path: '/sum1',
    component: lazy(() => import('../../views/pages/summary/summ')),
    exact: true
  },

  {
    path: '/sum2',
    component: lazy(() => import('../../views/pages/summary/summ2')),
    exact: true
  },

  {
    path: '/watertank',
    component: lazy(() => import('../../views/pages/dashboard/waterTank/waterTankOne')),
    exact: true
  },


  {
    path: '/coming-soon',
    component: lazy(() => import('../../views/pages/misc/ComingSoon')),
    layout: 'BlankLayout',
    meta: {
      publicRoute: true
    }

  },

  {
    path: ['bar/details/busbar','/bar/details/busbar/jk2','/bar/details/busbar/jk3'],
    component: lazy(() => import('../../views/pages/dashboard/table/details2/busbar/BarDetail')),
    exact: true
  },
  {
    path: '/begin',
    component: lazy(() => import('../../views/pages/misc/switch')),
    layout: 'BlankLayout'
  },
  {
    path: ['/idf/jk1','/idf/jk2','/idf/jk3'],
    component: lazy(() => import('../../views/pages/dashboard/table/details2/idf/BarDetail')),
    exact: true
  }


]

export default DashboardRoutes
