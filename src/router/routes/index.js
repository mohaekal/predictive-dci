// ** Routes Imports
import AppRoutes from './Apps'
import FormRoutes from './Forms'
import PagesRoutes from './Pages'
import TablesRoutes from './Tables'
import ChartMapsRoutes from './ChartsMaps'
import DashboardRoutes from './Dashboards'
import UiElementRoutes from './UiElements'
import ExtensionsRoutes from './Extensions'
import PageLayoutsRoutes from './PageLayouts'

// ** Document title
const TemplateTitle = '%s - Predictive'

// ** Default Route
const DefaultRoute = '/begin'
// const DefaultRoute = '/cpos/jk3/Chiller%201-1'

// ** Merge Routes
const Routes =DashboardRoutes

export { DefaultRoute, TemplateTitle, Routes }
