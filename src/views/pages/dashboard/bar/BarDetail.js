import React, { Component, Suspense } from "react"
import { Row, Col } from "reactstrap"
import Breadcrumbs from "../../../../@core/components/breadcrumbs";

import TabBar from "./TableBarDetails"
import Spinner from "reactstrap/es/Spinner";



let $primary = "#7367F0",
  $danger = "#EA5455",
  $warning = "#FF9F43",
  $info = "#00cfe8",
  $primary_light = "#9c8cfc",
  $warning_light = "#FFC085",
  $danger_light = "#f29292",
  $info_light = "#1edec5",
  $stroke_color = "#e8e8e8",
  $label_color = "#e7eef7",
  $white = "#fff"

  const CustomLoader = () => (
    <div style={{ padding: '2px' }}>
      <Spinner />
    </div>
  );

class BarDetail extends React.Component {
  render() {
    return (
      <React.Fragment>
          <Breadcrumbs
          breadCrumbTitle="Bar Details"
          breadCrumbParent="CRAC Bar Details"
        />
        <Row className="match-height">
       
          <Col lg="12" md="12">
 
<TabBar />
          </Col>


        </Row>
      </React.Fragment>
    )
  }
}



export default BarDetail