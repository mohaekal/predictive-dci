import React from "react"
import { Row, Col, Card, CardBody, CardTitle, Progress } from "reactstrap"
import { color } from 'd3-color';
import { interpolateRgb } from 'd3-interpolate';
import WaterTankAll from './waterTankAll'
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';
import FuelTankAll from "./fuelTankAll";
    const value = 0;
    const startColor = '#6495ed'; // cornflowerblue
    const endColor = '#dc143c'; // crimson
    const interpolate = interpolateRgb(startColor, endColor);
    const fillColor = interpolate(value / 100);
    const gradientStops = [
        {
            key: '0%',
            stopColor: color(fillColor).darker(0.5).toString(),
            stopOpacity: 1,
            offset: '0%'
        },
        {
            key: '50%',
            stopColor: fillColor,
            stopOpacity: 0.75,
            offset: '50%'
        },
        {
            key: '100%',
            stopColor: color(fillColor).brighter(0.5).toString(),
            stopOpacity: 0.5,
            offset: '100%'
        }
    ];


class waterTankOne extends React.Component {
    render() {
        return (

            <React.Fragment>
                <Col lg="9" md="9" sm="12" className="yrw">
                    <WaterTankAll />

                </Col>


            <Col lg="3" md="3 " sm="12"  className="yrw">

    <FuelTankAll />

    </Col>

            </React.Fragment>






        )
    }
}

export default waterTankOne
