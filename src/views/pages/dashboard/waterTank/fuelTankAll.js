import React, {Component, useEffect, useState} from "react"
import { Link } from "react-router-dom"
import DataTable from "react-data-table-component"
import Spinner from "../../../components/spinners/SpinnerFlex"
import { color } from 'd3-color';
import { interpolateRgb } from 'd3-interpolate';
import ReactDOM from 'react-dom';
import LiquidFillGauge from 'react-liquid-gauge';
import {
    Card, CardBody,
    CardFooter, CardTitle, Col, Row
} from "reactstrap"
import axios from "axios";
import Marquee from "react-fast-marquee";
import {AlertTriangle, Clock} from "react-feather";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faBuilding, faGasPump

} from "@fortawesome/free-solid-svg-icons";

const Gauge = ({ radius = 200, value = 0, ...props }) => {
    const startColor = '#07a819'; // cornflowerblue
    const endColor = '#36be33'; // crimson
    const interpolate = interpolateRgb(startColor, endColor);
    const fillColor = interpolate(value / 100);
    const gradientStops = [
        {
            key: '0%',
            stopColor: color(fillColor).darker(0.5).toString(),
            stopOpacity: 1,
            offset: '0%'
        },
        {
            key: '80%',
            stopColor: fillColor,
            stopOpacity: 0.75,
            offset: '80%'
        },
        {
            key: '100%',
            stopColor: color(fillColor).brighter(0.5).toString(),
            stopOpacity: 0.5,
            offset: '70%'
        }
    ];

    return (
        <LiquidFillGauge
            {...props}
            width={radius * 2}
            height={radius * 2}
            value={value}

            textSize={1}
            textOffsetX={0}
            textOffsetY={0}
            textRenderer={({ value, width, height, textSize, percent,wakwaw }) => {
                value = Math.round(value);
                const radius = Math.min(height / 2, width / 2);
                const textPixels = (textSize * radius / 2);
                const valueStyle = {
                    fontSize: textPixels
                };
                const percentStyle = {
                    fontSize: textPixels * 0.4
                };

                return (

                    <tspan>
                        <tspan className="value" style={valueStyle}>{wakwaw}</tspan>
                        <tspan style={percentStyle}>{percent}</tspan>
                    </tspan>
                );
            }}
            riseAnimation
            waveAnimation
            waveFrequency={1}
            waveAmplitude={1}
            gradient
            gradientStops={gradientStops}
            circleStyle={{
                fill: fillColor
            }}
            waveStyle={{
                fill: fillColor
            }}
            textStyle={{
                fill: color('#444').toString(),
                fontFamily: 'Arial'
            }}
            waveTextStyle={{
                fill: color('#fff').toString(),
                fontFamily: 'Arial'
            }}
        />
    );
};

class FuelTankAll extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
    }



    componentDidMount() {
        this.getBar();
        this.interval = setInterval(() => {
            this.getBar();
        }, 5000);
    }


    getBar() {

        axios.get('https://api-iot.dci-indonesia.com/predictive/api/all-sensor/by-group-id?groupid=208&gte=17-07-2021-06:00:00')


            .then(response => {
                const data = response.data.row;
                this.setState({ data })

            })

            .catch(function (error) {
                if (error.response) {
                    // Request made and server responded
                    console.log(error.response.data.row);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    // The request was made but no response was received
                    console.log(error.request);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }

            })



    }


    componentWillUnmount() {
        clearInterval(this.interval);
    }




    render() {
        const data = this.state.data
        const dataToShow = data
            ? data.filter(props => props.unit=== "Liter")
            : data

        return (

            <Row>


                {dataToShow.map((item, index,props) =>  <UserList key={props.val} {...item} />) }


            </Row>

        );
    }

}


const UserList = (props,error) => (
    <Col lg="12" sm="12"  className="">
        <Card className="last-cellx metal-nih">
            <p className="groupfueltank"><FontAwesomeIcon icon={faGasPump} className="primary f-s-13"  /> Fuel Tank</p>
            <p className="buildingtank"><FontAwesomeIcon icon={faBuilding} className="primary f-s-13"  /> {props.building}-{props.shortLevelName}</p>
            <CardBody>


                <CardTitle className="head-titlex metal-title"> {props.variableName}</CardTitle>

                <Gauge
                    style={{ margin: '0 auto 0px auto' }}
                    radius={60}
                    value={props.value/props.maxValue*100}
                    percent={props.unit}
                    wakwaw={props.value}
                    circleStyle= {props.type ===  "water" ? "{{fill: fillColor}}" :  "{{fill:'rgb(202,29,36)'}}"}


                    // value={this.state.value1}

                />

                <p className="last-time color-white" > <Clock size={11} style={{paddingBottom:1}}></Clock> {moment(props.convertedDatetime).calendar()} </p>
                <p className="max-tank color-white" > <AlertTriangle size={11} style={{paddingBottom:1}}></AlertTriangle> {props.maxValue} Liter </p>
            </CardBody>
        </Card>


    </Col>

)


export default FuelTankAll
