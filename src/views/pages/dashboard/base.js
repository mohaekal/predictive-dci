import React from "react"
import { Row, Col, Card, CardBody, CardTitle, Progress } from "reactstrap"
import TableBase from "./table/tableBase"

class Dashboard extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Row className="match-height">
          <Col lg="12" md="12">

          <TableBase />

          </Col>
        </Row>
      </React.Fragment>
    )
  }
}

export default Dashboard
