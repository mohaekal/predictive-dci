import { Link } from "react-router-dom"
import React, { Component,Suspense, lazy,Fragment } from 'react';
import axios from 'axios';

import {
  Card,
  CardBody,
  CardImg,
  CardTitle,
  Row,
  Col,
  Button,
  Progress
} from "reactstrap"

import Spinner from "reactstrap/es/Spinner";
import {AlertTriangle, Clock} from "react-feather";
import moment from "moment";
const loader2Style =
{position: "absolute",
  margin: "auto",
  width: "90%",
zIndex:"1"}

const CustomLoader = () => (
  <div style={loader2Style}>
    <Spinner />
  </div>
);

const pageLimit = 5;

class BarM extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true

    };
  }



  componentDidMount() {
    this.getBar();
    this.interval = setInterval(() => {
      this.getBar();
    }, 5000);
  }


  getBar() {

    axios.get('https://api-iot.dci-indonesia.com/predictive/api/all-sensor/supply-crac')


      .then(response => {
        const data = response.data.row;
        const data2 = response.data.group;
        this.setState({ data,loading: false })

      })

      .catch(function (error) {
        if (error.response) {
          // Request made and server responded
          console.log(error.response.data.row);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          // The request was made but no response was received
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log('Error', error.message);
        }

      })



  }


  componentWillUnmount() {
    clearInterval(this.interval);
  }




  render() {


    return (



      <Row>
        {this.state.loading ?

            <Fragment>

              <Col lg="3" sm="12"  className="">

                <div className="placeholder wave">

                  <div className="square"></div>

                </div>

              </Col>

              <Col lg="3" sm="12"  className="">

                <div className="placeholder wave">

                  <div className="square"></div>

                </div>

              </Col>

              <Col lg="3" sm="12"  className="">

                <div className="placeholder wave">

                  <div className="square"></div>

                </div>

              </Col>

              <Col lg="3" sm="12"  className="">

                <div className="placeholder wave">

                  <div className="square"></div>

                </div>

              </Col>

              <Col lg="3" sm="12"  className="">

                <div className="placeholder wave">

                  <div className="square"></div>

                </div>

              </Col>

              <Col lg="3" sm="12"  className="">

                <div className="placeholder wave">

                  <div className="square"></div>

                </div>

              </Col>

              <Col lg="3" sm="12"  className="">

                <div className="placeholder wave">

                  <div className="square"></div>

                </div>

              </Col>

              <Col lg="3" sm="12"  className="">

                <div className="placeholder wave">

                  <div className="square"></div>

                </div>

              </Col>
            </Fragment>


            : null}

        {this.state.data.map((item, index,props) => <UserList key={props.level} {...item} />)}


      </Row>

    );
  }
}
const color = "success "
const statuscomp = "comphidup"
const blinkNotif = "blink-baik"
const blinkNotif2 = "blink-baik"

const UserList = (props,error) => (

  <Col lg="3" sm="12"  className="">

    <Card >
      <CardBody>

        <div className= "">

          <CardTitle>{props.level}</CardTitle>
          <Fragment>
            {props.group.map(groups => {
              return <div>  <div className="text-center font-10">{groups.name}</div> <Progress  className="progress-lg " value={groups.data[0].value + 1 } color={groups.data[0].value >= groups.data[0].threshold && groups.data[0].threshold != null ? "danger" : "success"} >{groups.data[0].value}</Progress>

                <p className="last-time" > <Clock size={11} style={{paddingBottom:1}}></Clock> {moment(groups.data[0].convertedDatetime).calendar()} </p>

              </div> }  ) }
          </Fragment>



        </div>


      </CardBody>
    </Card>
  </Col>

)

export default BarM
