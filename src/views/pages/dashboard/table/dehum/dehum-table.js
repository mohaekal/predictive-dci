import React from "react"
import {Row, Col, Card, CardBody, CardTitle, Progress, CardImg, Button} from "reactstrap"

import DehumTable from "./DehumTable"
import DehumTableTop from "./DehumTable2"
import DehumTabBar from "../../dehum/dehum"


class DehumTableFront extends React.Component {
  render() {
    return (
      <React.Fragment>

        <Row className="match-height">

          <Col lg="3" md="12">
            <DehumTable />
          </Col>

            <Col lg="3" md="12">
                <DehumTableTop />
            </Col>

            <Col lg="12" md="12">
                <DehumTabBar />
            </Col>

        </Row>

    </React.Fragment>
    )
  }
}

export default DehumTableFront
