import React from "react"
import { Row, Col, Card, CardBody, CardTitle, Progress } from "reactstrap"
import Jkone from "./details/jk1"
import Jktwo from "./details/jk2"
import Jkthree from "./details/jk3"
class CposTableFront extends React.Component {
  render() {
    return (
      <React.Fragment>

        <Row className="match-height">

          <Col lg="3" md="12">
            <Jkone />
          </Col>

          <Col lg="3" md="12">
            <Jktwo />
          </Col>

          <Col lg="3" md="12">
            <Jkthree />

          </Col>

        </Row>
      </React.Fragment>
    )
  }
}

export default CposTableFront
