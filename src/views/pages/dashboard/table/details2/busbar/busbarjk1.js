import React,{ useState, useEffect } from "react"
import ReactDOM from "react-dom";
import DataTable from "react-data-table-component";
import orderBy from 'lodash/orderBy';
import Spinner from "../../../../../@core/components/spinner/Fallback-spinner"
import styled from 'styled-components';
import axios from 'axios';
import {
    Card,
    CardFooter,
    Button,
    CardHeader,
    CardTitle,
    Table,
    UncontrolledTooltip,
    Progress, CustomInput, CardBody
} from "reactstrap"
import Breadcrumbs from "../../../../../@core/components/breadcrumbs";
import moment from "moment";

const TextField = styled.input`
  height: 25px;
  width: 200px;
  border-radius: 3px;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
  border: 1px solid #e5e5e5;
  padding: 0 22px 0 16px;
  font-size: 12px;

  &:hover {
    cursor: pointer;
  }
`;




const FilterComponent = ({ filterText, onFilter, onClear }) => (
    <>

        <div>

            <div className='demo-inline-spacing'>
                <CustomInput type='radio' id='exampleCustomRadio' name='customRadio' inline label='All' value="" onChange={onFilter} defaultChecked />
                <CustomInput type='radio' id='exampleCustomRadio2' name='customRadio' inline label='Room A' value="Level - 1" onChange={onFilter} />
                <CustomInput type='radio' id='exampleCustomRadio3' name='customRadio' inline label='Room B' value="Level - 2" onChange={onFilter} />
                <CustomInput type='radio' id='exampleCustomRadio4' name='customRadio' inline label='Room C' value="Level - 3" onChange={onFilter} />

            </div>

        </div>
        <TextField id="search" type="text" placeholder="Search Here" value={filterText} onChange={onFilter} />
        {/* <ClearButton className="btn-primary" type="button" onClick={onClear}>X</ClearButton> */}
    </>
);



const customStyles = {

    cells: {
        style: {
            '&:not(:last-of-type)': {
                borderRightStyle: 'solid',
                borderRightWidth: '1px',
                borderRightColor: '#d8d8d8'
            },
        },
    },
};


const conditionalRowStyles1 = [
    {
        when: row => row.value1 >= 51 ,
        style: {
            backgroundColor: '#d4210f',
            color: 'white',
            '&:hover': {
                cursor: 'pointer',
            },
        },
    },





    {
        when: row => row.value1 < 51 ,
        style: {
            backgroundColor: '#28c76f',
            color: 'white',
            '&:hover': {
                cursor: 'pointer',
            },
        },
    },


];


const CustomLoader = () => (
    <div style={{ padding: '2px' }}>
        <Spinner />
    </div>
);


const columns = [
    {
        name: "Sensor Name",
        selector: "variableName",
        sortable: false,
        grow:8

    },
    {
        name: "Location",
        selector: "levelName",
        sortable: true,

    },
    {
        name: "Value",
        selector: "value1",
        sortable: true,
        center:true,
        grow:7,
        maxWidth:'10px'


    },
    {
        name: "Date",
        selector: "convertedDatetime",
        sortable: true,
        center:true,
        minWidth:'40px'
    }
];



function BusbarTab(props) {
    const [data, setData] = useState([]);
    const [filterText, setFilterText] = React.useState('');
    const [pending, setPending,] = React.useState(true);
    const [rows, setRows] = React.useState([]);
    const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);
    const filteredItems = data.filter(item => item.variableName && item.variableName.toLowerCase().includes(filterText.toLowerCase()) || item.levelName && item.levelName.toLowerCase().includes(filterText.toLowerCase()));

    const subHeaderComponentMemo = React.useMemo(() => {
        const handleClear = () => {
            if (filterText) {
                setResetPaginationToggle(!resetPaginationToggle);
                setFilterText('');
            }
        };

        return <FilterComponent onFilter={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} />;
    }, [filterText, resetPaginationToggle]);





    useEffect(() => {



        const interval = setInterval(() => {
            fetch("https://api-iot.dci-indonesia.com/predictive/api/all-sensor/by-group-id?groupid=210&gte=17-07-2021-06:00:00")
                .then(res => res.json())
                .then(res => setData(res.row));
        }, 3000);


        return () => clearInterval(interval);
    }, []);

    React.useEffect(() => {
        const timeout = setTimeout(() => {
            setRows(data);
            setPending(false);
        }, 4000);
        return () => clearTimeout(timeout);
    }, []);




    return (

        <div className="App">
            <Breadcrumbs
                breadCrumbTitle="Busbar"
                breadCrumbParent="Busbar Table Details"
            />
            <Card>
                <DataTable paginationResetDefaultPage={true} subHeader   subHeaderComponent={subHeaderComponentMemo} paginationResetDefaultPage={resetPaginationToggle} pagination  dense noHeader={true} customStyles={customStyles} columns={columns} data={filteredItems}  progressPending={pending}  progressComponent={<CustomLoader />}  conditionalRowStyles={conditionalRowStyles1} />
            </Card>

        </div>
    );

};


export default BusbarTab





