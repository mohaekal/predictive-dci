import React,{ useState, useEffect } from "react"
import ReactDOM from "react-dom";
import DataTable from "react-data-table-component";
import orderBy from 'lodash/orderBy';
import Spinner from "../../../../../@core/components/spinner/Fallback-spinner"
import styled from 'styled-components';

import axios from 'axios';
import {
    Card,
    CardFooter,
    Button,
    CardHeader,
    CardTitle,
    Table,
    UncontrolledTooltip,
    Progress
} from "reactstrap"
import logo from "../../../../../assets/images/logo/logo.png";
import comingSoonImg from '@src/assets/images/pages/coming-soon.svg'

import '@styles/base/pages/page-misc.scss'
import {Link} from "react-router-dom";



const TextField = styled.input`
  height: 25px;
  width: 200px;
  border-radius: 3px;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
  border: 1px solid #e5e5e5;
  padding: 0 22px 0 16px;
  font-size: 12px;

  &:hover {
    cursor: pointer;
  }
`;

const ClearButton = styled(Button)`
  padding: 0 22px 0 16px;
  margin: 20px ;
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
  height: 22px;
  width: 22px;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
`;


const FilterComponent = ({ filterText, onFilter, onClear }) => (
    <>
        <TextField id="search" type="text" placeholder="Filter By Counter id" value={filterText} onChange={onFilter} />
        {/* <ClearButton className="btn-primary" type="button" onClick={onClear}>X</ClearButton> */}
    </>
);











const customStyles = {

    cells: {
        style: {
            '&:not(:last-of-type)': {
                borderRightStyle: 'solid',
                borderRightWidth: '1px',
                borderRightColor: '#d8d8d8'
            },
'tr:nth-child(odd)':{backgroundColor: '#D9E1F2'},
        },
    },
};





const CustomLoader = () => (
    <div style={{ padding: '2px' }}>
        <Spinner />
    </div>
);


const columns = [
    {
        name: "COUNTER ID",
        selector: "counterId",
        sortable: true,



    },
    {
        name: "Prediction",
        selector: "riskPrediction",
        sortable: true,




    },
    {
        name: "Building",
        selector: "building",
        sortable: true,
    },
    {
        name: "Floor",
        selector: "floor",
        sortable: true,
    },
    {
        name: "Room",
        selector: "room",
        sortable: true,
    } ,

    {
        name: "Prediction Time",
        selector: "predictionDatetime",
        sortable: true,
    },

    {
        name: 'Poster Link',
        button: true,
        cell: row => <a href={row.imageUrl} target="_blank" rel="noopener noreferrer">View</a>,
    },
];








function allAllert(props) {
    const [data, setData] = useState([]);
    const [filterText, setFilterText] = React.useState('');
    const [pending, setPending,] = React.useState(true);
    const [rows, setRows] = React.useState([]);
    const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);
    const filteredItems = data.filter(item => item.counterId && item.counterId.toLowerCase().includes(filterText.toLowerCase()));

    const subHeaderComponentMemo = React.useMemo(() => {
        const handleClear = () => {
            if (filterText) {
                setResetPaginationToggle(!resetPaginationToggle);
                setFilterText('');
            }
        };

        return <FilterComponent onFilter={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} />;
    }, [filterText, resetPaginationToggle]);



    // const interval = setInterval(() => {
    //   console.log('This will run every second!');
    //   fetch("https://api-iot.dci-indonesia.com/inrespredictive/web/sensorlist/showOverviewVariable?interval=10&groupId=205")
    //   .then(res => res.json())
    //   .then(res => setData(res.rows));
    // }, 3000);

    useEffect(() => {
        fetch("https://api-iot.dci-indonesia.com/predictive/api/aivision/activity")
            .then(res => res.json())
            .then(res => setData(res.data));
        setInterval(100)
    }, []);













    React.useEffect(() => {
        const timeout = setTimeout(() => {
            setRows(data);
            setPending(false);
        }, 3000);
        return () => clearTimeout(timeout);
    }, []);






    return (
        <div className='misc-wrapper misc-20'>

            <div className='misc-inner p-2 p-sm-3'>
                <div className='w-100 text-center'>
                    <h2 className='mb-1'>We are launching soon 🚀</h2>
                    <p className='mb-3'>We're creating something awesome</p>
                    <Button tag={Link} to='/' color='primary' className='btn-sm-block mb-2'>
                        Back to home
                    </Button>

                </div>
            </div>
        </div>
    );

};


export default allAllert





