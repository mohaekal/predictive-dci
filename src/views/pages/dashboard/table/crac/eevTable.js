import React, { useState, useEffect } from "react"
import { Link } from "react-router-dom"
import DataTable from "react-data-table-component"
import Spinner from "../../../../../@core/components/spinner/Fallback-spinner"
import {
  Card,
  CardFooter,
  Button,
  CardHeader,
  CardTitle,
  Table,
  UncontrolledTooltip,
  Progress
} from "reactstrap"

const CustomLoader = () => (
  <div style={{ padding: '2px' }}>
    <Spinner />
  </div>
)

const columns = [
  {
    name: "Sensor Name",
    selector: "nm",
    sortable: false,
    grow:5,
    wrap : true

  },
  {
    name: "Value (BAR)",
    selector: "val",
    sortable: false,
    center:true,
    maxWidth:'10px'


  }
]

function EevTable(index) {
  const [data, setData] = useState([])
  const [pending, setPending] = React.useState(true)
  const [rows, setRows] = React.useState([])
  const url = "https://api-iot.dci-indonesia.com/predictive/api/crac/sort/bottom/suct-pressure"

  useEffect(() => {

    fetch(url).then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('Something went wrong');
      }
    })
        .then(res => setData(res.row))
        .catch((error) => {
          window.setTimeout('alert("Backend is not ready yet "+error+" Press OK to reload the page ");window.close();', 5000);
          window.location.reload();
        });




  }, [])

  React.useEffect(() => {
    const timeout = setTimeout(() => {
      setRows(data)
      setPending(false)
    }, 1200)
    return () => clearTimeout(timeout)
  }, [])

  return  (
    <div className="App"key={index.eevlow} >
      <Card className="test">
      <DataTable   title="Top 5 Suc Pressure Low" columns={columns} data={data} pagination paginationPerPage="5"  striped  defaultSortField="val" defaultSortAsc={true} progressPending={pending}  progressComponent={<CustomLoader />} />
      <CardFooter className="text-muted" style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
      <p><Link to='/bar/details/Suc_Pressure'>See More..</Link></p>

      </CardFooter>
      </Card>
    </div>

  )

}


export default EevTable