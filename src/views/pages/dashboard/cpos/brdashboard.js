import React from "react"
import {
  Breadcrumb,
  BreadcrumbItem,
  UncontrolledButtonDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle
} from "reactstrap"
import {Calendar, CheckSquare, Grid, Home, Mail, MessageSquare, Server,Settings} from "react-feather"
import {Link, NavLink} from "react-router-dom"
class BreadCrumbsCpos extends React.Component {
  render() {
    return (
      <div className="content-header row">
        <div className="content-header-left col-md-9 col-12">
          <div className="row breadcrumbs-top">
            <div className="col-12">
              {this.props.breadCrumbTitle ? (
                <h2 className="content-header-title float-left mb-0">
                  {this.props.breadCrumbTitle}
                </h2>
              ) : (
                ""
              )}
              <div className="breadcrumb-wrapper vx-breadcrumbs d-sm-block d-none col-12">
                <Breadcrumb tag="ol">
                  <BreadcrumbItem tag="li">
                    <NavLink to="/dashboard">
                      <Home className="align-top" size={15} />
                    </NavLink>
                  </BreadcrumbItem>
                  <BreadcrumbItem tag="li" className="text-primary">
                    <NavLink to="/dashboard/CPOS">
                    {this.props.breadCrumbParent}
                    </NavLink>
                  </BreadcrumbItem>
                  {this.props.breadCrumbParent2 ? (
                    <BreadcrumbItem tag="li" className="text-primary">
                      {this.props.breadCrumbParent2}
                    </BreadcrumbItem>
                  ) : (
                    ""
                  )}
                  {this.props.breadCrumbParent3 ? (
                    <BreadcrumbItem tag="li" className="text-primary">
                      {this.props.breadCrumbParent3}
                    </BreadcrumbItem>
                  ) : (
                    ""
                  )}
                  <BreadcrumbItem tag="li" active>
                    {this.props.breadCrumbActive}
                  </BreadcrumbItem>
                </Breadcrumb>
              </div>
            </div>
          </div>
        </div>
        <div className='content-header-right text-md-right col-md-3 col-12 d-md-block d-none'>
          <div className='form-group breadcrum-right dropdown'>
            <UncontrolledButtonDropdown>
              <DropdownToggle color='primary' size='sm' className='btn-icon btn-round dropdown-toggle'>
                <Grid size={14} />
              </DropdownToggle>
              <DropdownMenu tag='ul' right>
                <DropdownItem tag={Link} to='/cpos/jk1/'>
                  <Server className='mr-1' size={14} />
                  <span className='align-middle'>JK-1</span>
                </DropdownItem>
                <DropdownItem tag={Link} to='/cpos/jk2/'>
                  <Server className='mr-1' size={14} />
                  <span className='align-middle'>JK-2</span>
                </DropdownItem>
                <DropdownItem tag={Link} to='/cpos/jk3/'>
                  <Server className='mr-1' size={14} />
                  <span className='align-middle'>JK-3</span>
                </DropdownItem>

              </DropdownMenu>
            </UncontrolledButtonDropdown>
          </div>
        </div>
      </div>
    )
  }
}
export default BreadCrumbsCpos
