import React, {Component, Fragment, useEffect, useState} from "react"
import { Link } from "react-router-dom"
import DataTable from "react-data-table-component"
import Spinner from "../../../components/spinners/SpinnerFlex"
import { color } from 'd3-color';
import { interpolateRgb } from 'd3-interpolate';
import ReactDOM from 'react-dom';
import LiquidFillGauge from 'react-liquid-gauge';
import {
    Card, CardBody,
    CardFooter, CardTitle, Col, Row
} from "reactstrap"
import axios from "axios";
import Marquee from "react-fast-marquee";
import {AlertTriangle, Clock, PhoneCall} from 'react-feather'
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faBuilding,
    faThermometer,
    faTachometerAlt,
    faWater
} from "@fortawesome/free-solid-svg-icons";
import GaugeChart from 'react-gauge-chart';



class OutdoorTempAll extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading : true
        };
    }



    componentDidMount() {
        this.getBar();
        this.interval = setInterval(() => {
            this.getBar();
        }, 5000);
    }


    getBar() {

        axios.get('https://api-iot.dci-indonesia.com/predictive/api/all-sensor/by-group?groupName=Ruuvi%20Outdoor%20Temperature')


            .then(response => {
                const data = response.data.row;
                this.setState({ data, loading:false })

            })

            .catch(function (error) {
                if (error.response) {
                    // Request made and server responded
                    console.log(error.response.data.row);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    // The request was made but no response was received
                    console.log(error.request);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }

            })



    }


    componentWillUnmount() {
        clearInterval(this.interval);
    }




    render() {


        return (

            <Row>

                {this.state.loading ?

                    <Fragment>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>
                    </Fragment>


                    : null}
                {this.state.data.map((item, index,props) => <UserList key={props.value} {...item} />)}


            </Row>

        );
    }

}


const UserList = (props,error) => (
    <Col lg="3" sm="12"  className="zero-seven-rem">
        <Card className="last-cellx">
            <p className={props.value > props.threshold ? "temperatureOutdoorAlert" : "temperatureOutdoor" }><FontAwesomeIcon icon={faThermometer} className="primary f-s-13"  /> Temperature</p>
            <p className="buildingtank"><FontAwesomeIcon icon={faBuilding} className="primary f-s-13"  /> {props.building}-{props.shortLevelName}</p>
            <CardBody>


                <CardTitle className="head-titlex"> {props.variableName}</CardTitle>

                <h1 className={props.value > props.threshold ? "outdoor-temp-red bg-shadow" : "outdoor-temp-green bg-shadow" }>{props.value}</h1>

                <p className="last-time" > <Clock size={11} style={{paddingBottom:1}}></Clock> {moment(props.convertedDatetime).calendar()} </p>
                <p className="max-tank" > <AlertTriangle size={11} style={{paddingBottom:1}}></AlertTriangle> {props.threshold} °C </p>
            </CardBody>
        </Card>


    </Col>

)


export default OutdoorTempAll
