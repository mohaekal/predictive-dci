import {Button, DropdownItem} from 'reactstrap'
import { Link } from 'react-router-dom'
import notAuthImg from '@src/assets/images/pages/not-authorized.svg'
import edge from '@src/assets/images/logo/edge.png'
import dci from '@src/assets/images/logo/logo2.png'
import {
    Card, CardBody,
    CardFooter, CardTitle, Col, Row
} from "reactstrap"
import '@styles/base/pages/page-misc.scss'
import logo from "../../../assets/images/logo/logo.png";
import React, {useEffect, useState} from "react";
import { isUserLoggedIn } from '@utils'

// ** Store & Actions
import { useDispatch } from 'react-redux'
import { handleLogout } from '@store/actions/auth'
const SwitchV = () => {

    const dispatch = useDispatch()

    // ** State
    const [userData, setUserData] = useState(null)

    //** ComponentDidMount
    useEffect(() => {
        if (isUserLoggedIn() !== null) {
            setUserData(JSON.parse(localStorage.getItem('userData')))
        }
    }, [])


    return (
    <div className='misc-wrapper'>
      <a className='brand-logo' href='/'>
        <img src={logo}/>
        <h2 className='brand-text text-primary ml-1'></h2>
      </a>
      <div className='misc-inner p-2 p-sm-3'>
        <div className='w-100 text-center'>
          <h2 className='mb-1'>Welcome Back {(userData && userData['username']) || 'John Doe'} !</h2>
          <p className='mb-2'>
            The Predictive DCI website in IIS uses the default IUSR account credentials to access the web pages
            it serves.
          </p>
            <Row>
            <Col lg="6" sm="12"  className="ib" tag={Link} to='/cpos/jk3/Chiller%201-1'>
                <Card lg="6" className="">
                    <CardBody>
                        <img src={dci}/>

                    </CardBody>
                </Card>


            </Col>

                <Col lg="6" sm="12"  className="ib" tag={Link} to='/cpos/jk3/Chiller%201-2'>
                    <Card lg="6" className="">
                        <CardBody>
                            <img src={edge}/>

                        </CardBody>
                    </Card>


                </Col>

            </Row>
          <img className='img-fluid abs' src={notAuthImg} alt='Not authorized page' />
            {/*<Button tag={Link} to='/login' onClick={() => dispatch(handleLogout())} color='primary' className='btn-sm-block mb-1'>*/}
            {/*   Logout*/}
            {/*</Button>*/}
        </div>
      </div>
    </div>
  )
}
export default SwitchV
