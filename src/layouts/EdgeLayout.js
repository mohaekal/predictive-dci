// !Do not remove the Layout import
import Layout from '@layouts/VerticalLayout'

const EdgeLayout = props => <Layout {...props}>{props.children}</Layout>

export default EdgeLayout
